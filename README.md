# exSaddle
By Dave A. May (@dmay) and Patrick Sanan (@psanan).

PETSc codes to test preconditioners for Stokes and Lame' saddle-point systems.

This includes a family of Q2-Q1 (Taylor-Hood) codes for Stokes and mixed-form linear elasticity systems, suitable for tests with monolithic preconditioners.

You will also find modifications of PETSc KSP tutorial examples ex23 and ex42.

Requires PETSc 3.12 or later.
